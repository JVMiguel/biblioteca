using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace biblioteca.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class RevistaController : ControllerBase
  {
    private readonly Contexto _context;

    public RevistaController (Contexto context)
    {
        _context = context;
    }

    // GET api/revista
    [HttpGet]
    public ActionResult Get()
    {
        return Ok(_context.Revistas);
    }

    [HttpGet("{Id}")]
    public ActionResult getById([FromRoute] int id){

      return Ok(_context.Revistas.Where(x=>x.Id.Equals(id)).FirstOrDefault());
    }

    [HttpPost]
    public ActionResult Post([FromBody] Revista revista){

      try
      {
        _context.Revistas.Add(revista);
        _context.SaveChanges();
        return Ok(revista.Id);
      }
      catch (System.Exception e)
      {
        System.Console.WriteLine("Deu ruim na inserção: {0}", e.StackTrace);   
        return BadRequest();      
      }
    }

    [HttpPut("{Id}")]
    public IActionResult Put(int id, Revista revista){
      if (id != revista.Id){
        return BadRequest();
      }

      _context.Entry(revista).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    [HttpDelete("{Id}")]
    public IActionResult Delete(int id){
      var revista = _context.Revistas.Find(id);

      if (revista == null){
        return NotFound();
      }

      _context.Revistas.Remove(revista);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}