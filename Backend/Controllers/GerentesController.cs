using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace biblioteca.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class GerenteController : ControllerBase
  {
    private readonly Contexto _context;

    public GerenteController(Contexto context)
    {
        _context = context;
    }

    // GET api/gerente
    [HttpGet]
    public ActionResult Get()
    {
        return Ok(_context.Gerentes);
    }

    [HttpGet("{Id}")]
    public ActionResult getById([FromRoute] int id){

      return Ok(_context.Gerentes.Where(x=>x.Id.Equals(id)).FirstOrDefault());
    }

    [HttpPost]
    public ActionResult Post([FromBody] Gerente gerente){

      try
      {
        _context.Gerentes.Add(gerente);
        _context.SaveChanges();
        return Ok(gerente.Id);
      }
      catch (System.Exception e)
      {
        System.Console.WriteLine("Deu ruim na inserção: {0}", e.StackTrace);   
        return BadRequest();      
      }
    }

    [HttpPut("{Id}")]
    public IActionResult Put(int id, Gerente gerente){
      if (id != gerente.Id){
        return BadRequest();
      }

      _context.Entry(gerente).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    [HttpDelete("{Id}")]
    public IActionResult Delete(int id){
      var gerente = _context.Gerentes.Find(id);

      if (gerente == null){
        return NotFound();
      }

      _context.Gerentes.Remove(gerente);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
