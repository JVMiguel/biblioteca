using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace biblioteca.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class LocacaoController : ControllerBase
  {
    private readonly Contexto _context;

    public LocacaoController(Contexto context)
    {
        _context = context;
    }

    // GET api/locacao
    [HttpGet]
    public ActionResult Get()
    {
        return Ok(_context.Locacoes);
    }

    [HttpGet("{Id}")]
    public ActionResult getById([FromRoute] int id){

      return Ok(_context.Locacoes.Where(x=>x.Id.Equals(id)).FirstOrDefault());
    }

    [HttpPost]
    public ActionResult Post([FromBody] Locacao locacao){

      try
      {
        _context.Locacoes.Add(locacao);
        _context.SaveChanges();
        return Ok(locacao.Id);
      }
      catch (System.Exception e)
      {
        System.Console.WriteLine("Deu ruim na inserção: {0}", e.StackTrace);   
        return BadRequest();      
      }
    }

    [HttpPut("{Id}")]
    public IActionResult Put(int id, Locacao locacao){
      if (id != locacao.Id){
        return BadRequest();
      }

      _context.Entry(locacao).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    [HttpDelete("{Id}")]
    public IActionResult Delete(int id){
      var locacao = _context.Locacoes.Find(id);

      if (locacao == null){
        return NotFound();
      }

      _context.Locacoes.Remove(locacao);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
