using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace biblioteca.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class AlunoController : ControllerBase
  {
    private readonly Contexto _context;

    public AlunoController(Contexto context)
    {
        _context = context;
    }

    // GET api/aluno
    [HttpGet]
    public ActionResult Get()
    {
        return Ok(_context.Alunos);
    }

    [HttpGet("{Id}")]
    public ActionResult getById([FromRoute] string Id){

      return Ok(_context.Alunos.Where(x=>x.Cpf.Equals(Id)).FirstOrDefault());
    }

    [HttpPost]
    public ActionResult Post([FromBody] Aluno aluno){

      try
      {
        _context.Alunos.Add(aluno);
        _context.SaveChanges();
        return Ok(aluno.Cpf);
      }
      catch (System.Exception e)
      {
        System.Console.WriteLine("Deu ruim na inserção: {0}", e.StackTrace);   
        return BadRequest();      
      }
    }

    [HttpPut("{Id}")]
    public IActionResult Put(string Id, Aluno aluno){
      if (Id != aluno.Cpf){
        return BadRequest();
      }

      _context.Entry(aluno).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    [HttpDelete("{Id}")]
    public IActionResult Delete(string id){
      var aluno = _context.Alunos.Find(id);

      if (aluno == null){
        return NotFound();
      }

      _context.Alunos.Remove(aluno);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}