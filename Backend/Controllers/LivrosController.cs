using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace biblioteca.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class LivroController : ControllerBase
  {
    private readonly Contexto _context;

    public LivroController (Contexto context)
    {
        _context = context;
    }

    // GET api/livro
    [HttpGet]
    public ActionResult Get()
    {
        return Ok(_context.Livros);
    }

    [HttpGet("{Id}")]
    public ActionResult getById([FromRoute] int id){

      return Ok(_context.Livros.Where(x=>x.Id.Equals(id)).FirstOrDefault());
    }

    [HttpPost]
    public ActionResult Post([FromBody] Livro livro){

      try
      {
        _context.Livros.Add(livro);
        _context.SaveChanges();
        return Ok(livro.Id);
      }
      catch (System.Exception e)
      {
        System.Console.WriteLine("Deu ruim na inserção: {0}", e.StackTrace);   
        return BadRequest();      
      }
    }

    [HttpPut("{Id}")]
    public IActionResult Put(int id, Livro livro){
      if (id != livro.Id){
        return BadRequest();
      }

      _context.Entry(livro).State = EntityState.Modified;
      _context.SaveChanges();

      return NoContent();
    }

    [HttpDelete("{Id}")]
    public IActionResult Delete(int id){
      var livro = _context.Livros.Find(id);

      if (livro == null){
        return NotFound();
      }

      _context.Livros.Remove(livro);
      _context.SaveChangesAsync();

      return NoContent();
    }
  }
}
