﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Backend.Migrations
{
    [DbContext(typeof(Contexto))]
    [Migration("20191201005607_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Aluno", b =>
                {
                    b.Property<string>("Cpf")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Curso");

                    b.Property<int>("Matricula");

                    b.Property<string>("Nome");

                    b.HasKey("Cpf");

                    b.ToTable("Alunos");
                });

            modelBuilder.Entity("Gerente", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Cpf");

                    b.Property<string>("nome");

                    b.HasKey("Id");

                    b.ToTable("Gerentes");
                });

            modelBuilder.Entity("Livro", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Auto");

                    b.Property<string>("Edicao");

                    b.Property<string>("Editora");

                    b.Property<string>("Titulo");

                    b.Property<int>("anoPublicacao");

                    b.HasKey("Id");

                    b.ToTable("Livros");
                });

            modelBuilder.Entity("Locacao", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("alunoId");

                    b.Property<DateTime>("dataEntrega");

                    b.Property<DateTime>("dataLocacao");

                    b.Property<int>("livroId");

                    b.HasKey("Id");

                    b.ToTable("Locacoes");
                });

            modelBuilder.Entity("Revista", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Edicao");

                    b.Property<string>("Editora");

                    b.Property<string>("Titulo");

                    b.Property<int>("anoPublicacao");

                    b.HasKey("Id");

                    b.ToTable("Revistas");
                });
#pragma warning restore 612, 618
        }
    }
}
