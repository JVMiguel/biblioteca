using System.ComponentModel.DataAnnotations;

public class Revista {
    [Key]
    public int Id { get; set; }
    public string Titulo { get; set; }
    public int anoPublicacao { get; set; }
    public string Edicao { get; set; }
    public string Editora { get; set; }
}