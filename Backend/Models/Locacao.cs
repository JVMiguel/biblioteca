using System;
using System.ComponentModel.DataAnnotations;

public class Locacao {
    public string alunoId { get; set; }
    [Key]
    public int Id { get; set; }
    public DateTime dataLocacao { get; set; }
    public DateTime dataEntrega { get; set; }
    public int livroId { get; set; }

}