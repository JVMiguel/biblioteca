using Microsoft.EntityFrameworkCore;

  public class Contexto : DbContext {
    public Contexto(DbContextOptions<Contexto> options)
        : base(options){
            
        }
    public DbSet<Aluno> Alunos { get; set; }
    public DbSet<Gerente> Gerentes { get; set; }
    public DbSet<Livro> Livros { get; set; }
    public DbSet<Locacao> Locacoes { get; set; }
    public DbSet<Revista> Revistas { get; set; }

  }