import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './Cadastros.css';

export class Livros extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            titulo: '',
            autor: '',
            anoPublicacao: 0,
            edicao: '',
            editora: '',
            modalAberta: false,
            livros: []
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.cancelar = this.cancelar.bind(this);
        this.handleTituloChange = this.handleTituloChange.bind(this);
        this.handleAutorChange = this.handleAutorChange.bind(this);
        this.handleAnoPublicacaoChange = this.handleAnoPublicacaoChange.bind(this);
        this.handleEdicaoChange = this.handleEdicaoChange.bind(this);
        this.handleEditoraChange = this.handleEditoraChange.bind(this);
        this.fecharModal = this.fecharModal.bind(this);
        this.abrirModalInserir = this.abrirModalInserir.bind(this);
        this.abrirModalAtualizar = this.abrirModalAtualizar.bind(this);

    }

    UNSAFE_componentWillMount() {
        this.buscarLivros();
    }

    buscarLivros() {
        fetch('/api/livro')
            .then(response => response.json())
            .then(data => this.setState({ livros: data }));
    }

    buscarLivro(id) {
        fetch('/api/livro/' + id)
            .then(response => response.json())
            .then(data => this.setState(
                {
                    id: data.id,
                    titulo: data.titulo,
                    autor: data.autor,
                    anoPublicacao: data.anoPublicacao,
                    edicao: data.edicao,
                    editora: data.editora
                }));
    }

    inserirLivro(livro) {
        console.log(JSON.stringify(livro));
        fetch('/api/livro/', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(livro)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarLivros();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    atualizarLivro(livro) {
        fetch('/api/livro/' + livro.id, {
            method: 'put',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(livro)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarLivros();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    excluirLivro(id) {
        fetch('/api/livro/' + id, {
            method: 'delete'
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarLivros();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        const livro = {
            id: this.state.id,
            titulo: this.state.titulo,
            autor: this.state.autor,
            anoPublicacao: this.state.anoPublicacao,
            edicao: this.state.edicao,
            editora: this.state.editora
        };

        if (this.state.id === 0) {
            this.inserirLivro(livro);
          } else {
            this.atualizarLivro(livro);
          }

    }

    cancelar() {
        this.setState({
            id: 0,
            titulo: '',
            autor: '',
            anoPublicacao: 0,
            edicao: '',
            editora: '',
            modalAberta: false,
            livros: []
        });
    }

    handleTituloChange(e) {
        this.setState({
            titulo: e.target.value
        });
    }
    
    handleAutorChange(e) {
        this.setState({
            autor: e.target.value
        });
    }

    handleAnoPublicacaoChange(e) {
        this.setState({
            anoPublicacao: e.target.value
        });
    }

    handleEdicaoChange(e) {
        this.setState({
            edicao: e.target.value
        });
    }

    handleEditoraChange(e) {
        this.setState({
            editora: e.target.value
        });
    }

    fecharModal() {
        this.setState({
            modalAberta: false
        })
    }

    abrirModalInserir() {
        this.setState({
            modalAberta: true
        })
    }

    abrirModalAtualizar(id) {
        this.setState({
            id: id,
            modalAberta: true
        });

        this.buscarLivro(id);
    }

    renderTabela() {
        let colunasTabela = [];
        for (var i = 0; i < this.state.livros.length; i++) {
            const livro = this.state.livros[i];
            const coluna = (
                <tr key={livro.id}>
                    <td>{livro.titulo}</td>
                    <td>{livro.autor}</td>
                    <td>{livro.anoPublicacao}</td>
                    <td>{livro.edicao}</td>
                    <td>{livro.editora}</td>
                    <td>
                        <div>
                            <Button variant="link" onClick={() => this.abrirModalAtualizar(livro.id)}>Atualizar</Button>
                            <Button variant="link" onClick={() => this.excluirLivro(livro.id)}>Excluir</Button>
                        </div>
                    </td>
                </tr>);
            colunasTabela.push(coluna);
        }
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Autor</th>
                        <th>Ano de Publicação</th>
                        <th>Edição</th>
                        <th>Editora</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {colunasTabela}
                </tbody>
            </Table>
        );
    }

    renderModal() {
        return (
            <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Inserir novo livro</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id="modalForm" onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <Form.Label>Título</Form.Label>
                            <Form.Control type='text' placeholder='Título do Livro' value={this.state.titulo} onChange={this.handleTituloChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Autor</Form.Label>
                            <Form.Control type='text' placeholder='Nome do Autor' value={this.state.autor} onChange={this.handleAutorChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Ano de Publicação</Form.Label>
                            <Form.Control type='number' placeholder='Ano em que foi publicado' value={this.state.anoPublicacao} onChange={this.handleAnoPublicacaoChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Edição</Form.Label>
                            <Form.Control type='text' placeholder='Edição' value={this.state.edicao} onChange={this.handleEdicaoChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Editora</Form.Label>
                            <Form.Control type='text' placeholder='Editora' value={this.state.editora} onChange={this.handleEditoraChange} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.cancelar}>
                        Cancelar
              </Button>
                    <Button variant="primary" form="modalForm" type="submit">
                        Confirmar
              </Button>
                </Modal.Footer>
            </Modal>
        );
    }

    render() {
        return (
            <div>
                <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Novo Livro</Button>
                {this.renderTabela()}
                {this.renderModal()}
            </div>
        );
    }
}