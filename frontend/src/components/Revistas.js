import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './Cadastros.css';

export class Revistas extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            titulo: '',
            anoPublicacao: 0,
            edicao: '',
            editora: '',
            modalAberta: false,
            revistas: []
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.cancelar = this.cancelar.bind(this);
        this.handleTituloChange = this.handleTituloChange.bind(this);
        this.handleAnoPublicacaoChange = this.handleAnoPublicacaoChange.bind(this);
        this.handleEdicaoChange = this.handleEdicaoChange.bind(this);
        this.handleEditoraChange = this.handleEditoraChange.bind(this);
        this.fecharModal = this.fecharModal.bind(this);
        this.abrirModalInserir = this.abrirModalInserir.bind(this);
        this.abrirModalAtualizar = this.abrirModalAtualizar.bind(this);

    }

    UNSAFE_componentWillMount() {
        this.buscarRevistas();
    }

    buscarRevistas() {
        fetch('/api/revista')
            .then(response => response.json())
            .then(data => this.setState({ revistas: data }));
    }

    buscarRevista(id) {
        fetch('/api/revista/' + id)
            .then(response => response.json())
            .then(data => this.setState(
                {
                    id: data.id,
                    titulo: data.titulo,
                    anoPublicacao: data.anoPublicacao,
                    edicao: data.edicao,
                    editora: data.editora
                }));
    }

    inserirRevista(revista) {
        console.log(JSON.stringify(revista));
        fetch('/api/revista/', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(revista)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarRevistas();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    atualizarRevista(revista) {
        fetch('/api/revista/' + revista.id, {
            method: 'put',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(revista)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarRevistas();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    excluirRevista(id) {
        fetch('/api/revista/' + id, {
            method: 'delete'
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarRevistas();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        const revista = {
            id: this.state.id,
            titulo: this.state.titulo,
            anoPublicacao: this.state.anoPublicacao,
            edicao: this.state.edicao,
            editora: this.state.editora
        };

        if (this.state.id === 0) {
            this.inserirRevista(revista);
          } else {
            this.atualizarRevista(revista);
          }

    }

    cancelar() {
        this.setState({
            id: 0,
            titulo: '',
            anoPublicacao: 0,
            edicao: '',
            editora: '',
            modalAberta: false,
            revistas: []
        });
    }

    handleTituloChange(e) {
        this.setState({
            titulo: e.target.value
        });
    }

    handleAnoPublicacaoChange(e) {
        this.setState({
            anoPublicacao: e.target.value
        });
    }

    handleEdicaoChange(e) {
        this.setState({
            edicao: e.target.value
        });
    }

    handleEditoraChange(e) {
        this.setState({
            editora: e.target.value
        });
    }

    fecharModal() {
        this.setState({
            modalAberta: false
        })
    }

    abrirModalInserir() {
        this.setState({
            modalAberta: true
        })
    }

    abrirModalAtualizar(id) {
        this.setState({
            id: id,
            modalAberta: true
        });

        this.buscarRevista(id);
    }

    renderTabela() {
        let colunasTabela = [];
        for (var i = 0; i < this.state.revistas.length; i++) {
            const revista = this.state.revistas[i];
            const coluna = (
                <tr key={revista.id}>
                    <td>{revista.titulo}</td>
                    <td>{revista.anoPublicacao}</td>
                    <td>{revista.edicao}</td>
                    <td>{revista.editora}</td>
                    <td>
                        <div>
                            <Button variant="link" onClick={() => this.abrirModalAtualizar(revista.id)}>Atualizar</Button>
                            <Button variant="link" onClick={() => this.excluirRevista(revista.id)}>Excluir</Button>
                        </div>
                    </td>
                </tr>);
            colunasTabela.push(coluna);
        }
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Ano de Publicação</th>
                        <th>Edição</th>
                        <th>Editora</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {colunasTabela}
                </tbody>
            </Table>
        );
    }

    renderModal() {
        return (
            <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Inserir nova revista</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id="modalForm" onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <Form.Label>Título</Form.Label>
                            <Form.Control type='text' placeholder='Título da revista' value={this.state.titulo} onChange={this.handleTituloChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Ano de Publicação</Form.Label>
                            <Form.Control type='number' placeholder='Ano em que foi publicado' value={this.state.anoPublicacao} onChange={this.handleAnoPublicacaoChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Edição</Form.Label>
                            <Form.Control type='text' placeholder='Edição' value={this.state.edicao} onChange={this.handleEdicaoChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Editora</Form.Label>
                            <Form.Control type='text' placeholder='Editora' value={this.state.editora} onChange={this.handleEditoraChange} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.cancelar}>
                        Cancelar
              </Button>
                    <Button variant="primary" form="modalForm" type="submit">
                        Confirmar
              </Button>
                </Modal.Footer>
            </Modal>
        );
    }

    render() {
        return (
            <div>
                <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Nova Revista</Button>
                {this.renderTabela()}
                {this.renderModal()}
            </div>
        );
    }
}