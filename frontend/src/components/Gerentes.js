import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './Cadastros.css';

export class Gerentes extends Component {

    constructor(props) {
        super(props);

        this.state = {
            id: 0,
            nome: '',
            cpf: '',
            modalAberta: false,
            gerentes: []
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.cancelar = this.cancelar.bind(this);
        this.handleNomeChange = this.handleNomeChange.bind(this);
        this.handleCpfChange = this.handleCpfChange.bind(this);
        this.fecharModal = this.fecharModal.bind(this);
        this.abrirModalInserir = this.abrirModalInserir.bind(this);
        this.abrirModalAtualizar = this.abrirModalAtualizar.bind(this);

    }

    UNSAFE_componentWillMount() {
        this.buscarGerentes();
    }

    buscarGerentes() {
        fetch('/api/gerente')
            .then(response => response.json())
            .then(data => this.setState({ gerentes: data }));
    }

    buscarGerente(id) {
        fetch('/api/gerente/' + id)
            .then(response => response.json())
            .then(data => this.setState(
                {
                    id: data.id,
                    nome: data.nome,
                    cpf: data.cpf
                }));
    }

    inserirGerente(gerente) {
        fetch('/api/gerente/', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(gerente)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarGerentes();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    atualizarGerente(gerente) {
        fetch('/api/gerente/' + gerente.id, {
            method: 'put',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(gerente)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarGerentes();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    excluirGerente(id) {
        fetch('/api/gerente/' + id, {
            method: 'delete'
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarGerentes();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        const gerente = {
            id: this.state.id,
            nome: this.state.nome,
            cpf: this.state.cpf
        };

        if (this.state.id === 0) {
            this.inserirGerente(gerente);
          } else {
            this.atualizarGerente(gerente);
          }

    }

    cancelar() {
        this.setState({
            id: 0,
            nome: '',
            cpf: '',
            modalAberta: false
        });
    }

    handleNomeChange(e) {
        this.setState({
            nome: e.target.value
        });
    }

    handleCpfChange(e) {
        this.setState({
            cpf: e.target.value
        });
    }

    fecharModal() {
        this.setState({
            modalAberta: false
        })
    }

    abrirModalInserir() {
        this.setState({
            modalAberta: true
        })
    }

    abrirModalAtualizar(id) {
        this.setState({
            id: id,
            modalAberta: true
        });

        this.buscarGerente(id);
    }

    renderTabela() {
        let colunasTabela = [];
        for (var i = 0; i < this.state.gerentes.length; i++) {
            const gerente = this.state.gerentes[i];
            const coluna = (
                <tr key={gerente.id}>
                    <td>{gerente.nome}</td>
                    <td>{gerente.cpf}</td>
                    <td>
                        <div>
                            <Button variant="link" onClick={() => this.abrirModalAtualizar(gerente.id)}>Atualizar</Button>
                            <Button variant="link" onClick={() => this.excluirGerente(gerente.id)}>Excluir</Button>
                        </div>
                    </td>
                </tr>);
            colunasTabela.push(coluna);
        }
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {colunasTabela}
                </tbody>
            </Table>
        );
    }

    renderModal() {
        return (
            <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Inserir novo gerente</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id="modalForm" onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <Form.Label>Nome</Form.Label>
                            <Form.Control type='text' placeholder='Nome do gerente' value={this.state.nome} onChange={this.handleNomeChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>CPF</Form.Label>
                            <Form.Control type='text' placeholder='Insira somente números' value={this.state.cpf} onChange={this.handleCpfChange} />
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.cancelar}>
                        Cancelar
              </Button>
                    <Button variant="primary" form="modalForm" type="submit">
                        Confirmar
              </Button>
                </Modal.Footer>
            </Modal>
        );
    }

    render() {
        return (
            <div>
                <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Novo Gerente</Button>
                {this.renderTabela()}
                {this.renderModal()}
            </div>
        );
    }
}