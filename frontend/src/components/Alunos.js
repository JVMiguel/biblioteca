import React, { Component } from 'react';
import { Button, Form, Modal, Table } from 'react-bootstrap';
import './Cadastros.css';

export class Alunos extends Component {

    constructor(props) {
        super(props);

        this.state = {
            matricula: 0,
            nome: '',
            cpf: '',
            curso: '',
            modalAberta: false,
            alunos: [],
            inserir: true
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.cancelar = this.cancelar.bind(this);
        this.handleMatriculaChange = this.handleMatriculaChange.bind(this);
        this.handleNomeChange = this.handleNomeChange.bind(this);
        this.handleCpfChange = this.handleCpfChange.bind(this);
        this.handleCursoChange = this.handleCursoChange.bind(this);
        this.fecharModal = this.fecharModal.bind(this);
        this.abrirModalInserir = this.abrirModalInserir.bind(this);
        this.abrirModalAtualizar = this.abrirModalAtualizar.bind(this);

    }

    UNSAFE_componentWillMount() {
        this.buscarAlunos();
    }

    buscarAlunos() {
        fetch('/api/aluno')
            .then(response => response.json())
            .then(data => this.setState({ alunos: data }));
    }

    buscarAluno(cpf) {
        fetch('/api/aluno/' + cpf)
            .then(response => response.json())
            .then(data => this.setState(
                {
                    cpf: data.cpf,
                    nome: data.nome,
                    curso: data.curso,
                    matricula: data.matricula
                }));
    }

    inserirAluno(aluno) {
        fetch('/api/aluno/', {
            method: 'post',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(aluno)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarAlunos();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    atualizarAluno(aluno) {
        fetch('/api/aluno/' + aluno.cpf, {
            method: 'put',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(aluno)
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarAlunos();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    excluirAluno(cpf) {
        fetch('/api/aluno/' + cpf, {
            method: 'delete'
        }).then((resposta) => {
            if (resposta.ok) {
                this.buscarAlunos();
                this.cancelar();
            } else {
                alert(JSON.stringify(resposta));
            }
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        const aluno = {
            cpf: this.state.cpf,
            curso: this.state.curso,
            nome: this.state.nome,
            matricula: this.state.matricula
        };

        if (this.state.inserir) {
            this.inserirAluno(aluno);
          } else {
            this.atualizarAluno(aluno);
          }

    }

    cancelar() {
        this.setState({
            matricula: 0,
            nome: '',
            cpf: '',
            curso: '',
            modalAberta: false
        });
    }

    handleMatriculaChange(e) {
        this.setState({
            matricula: e.target.value
        });
    }

    handleNomeChange(e) {
        this.setState({
            nome: e.target.value
        });
    }

    handleCpfChange(e) {
        this.setState({
            cpf: e.target.value
        });
    }

    handleCursoChange(e) {
        this.setState({
            curso: e.target.value
        });
    }

    fecharModal() {
        this.setState({
            modalAberta: false
        })
    }

    abrirModalInserir() {
        this.setState({
            matricula: 0,
            nome: '',
            cpf: '',
            curso: '',
            modalAberta: true,
            inserir: true
        })
    }

    abrirModalAtualizar(cpf) {
        console.log(cpf);
        this.setState({
            cpf: cpf,
            modalAberta: true,
            inserir: false
        });

        this.buscarAluno(cpf);
    }

    renderTabela() {
        let colunasTabela = [];
        for (var i = 0; i < this.state.alunos.length; i++) {
            const aluno = this.state.alunos[i];
            const coluna = (
                <tr key={aluno.cpf}>
                    <td>{aluno.cpf}</td>
                    <td>{aluno.nome}</td>
                    <td>{aluno.matricula}</td>
                    <td>{aluno.curso}</td>
                    <td>
                        <div>
                            <Button variant="link" onClick={() => this.abrirModalAtualizar(aluno.cpf)}>Atualizar</Button>
                            <Button variant="link" onClick={() => this.excluirAluno(aluno.cpf)}>Excluir</Button>
                        </div>
                    </td>
                </tr>);
            colunasTabela.push(coluna);
        }
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>CPF</th>
                        <th>Nome</th>
                        <th>Matrícula</th>
                        <th>Curso</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    {colunasTabela}
                </tbody>
            </Table>
        );
    }

    renderModal() {
        var controlCpf = <Form.Control type='text' disabled placeholder='Insira somente números' value={this.state.cpf} onChange={this.handleCpfChange} />;
        if (this.state.inserir){
            controlCpf = <Form.Control type='text' placeholder='Insira somente números' value={this.state.cpf} onChange={this.handleCpfChange} />
        }
        return (
            <Modal show={this.state.modalAberta} onHide={this.fecharModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Inserir novo aluno</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form id="modalForm" onSubmit={this.handleSubmit}>
                        <Form.Group>
                            <Form.Label>Matrícula</Form.Label>
                            <Form.Control type='number' placeholder='Matrícula do aluno' value={this.state.matricula} onChange={this.handleMatriculaChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Nome</Form.Label>
                            <Form.Control type='text' placeholder='Nome do aluno' value={this.state.nome} onChange={this.handleNomeChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Curso</Form.Label>
                            <Form.Control type='text' placeholder='Curso' value={this.state.curso} onChange={this.handleCursoChange} />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>CPF</Form.Label>
                            {controlCpf}
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.cancelar}>
                        Cancelar
              </Button>
                    <Button variant="primary" form="modalForm" type="submit">
                        Confirmar
              </Button>
                </Modal.Footer>
            </Modal>
        );
    }

    render() {
        return (
            <div>
                <Button variant="primary" className="button-novo" onClick={this.abrirModalInserir}>Novo Aluno</Button>
                {this.renderTabela()}
                {this.renderModal()}
            </div>
        );
    }
}