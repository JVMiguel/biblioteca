import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom'
import { Home } from './components/Home';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { Alunos } from './components/Alunos';
import { Gerentes } from './components/Gerentes';
import { Livros } from './components/Livros';
import { Revistas } from './components/Revistas';


class App extends Component {
  render() {
    return (
      <Container>
        <BrowserRouter>
          <Navbar bg='light' expand='lg'>
            <Navbar.Brand as={Link} to="/">Biblioteca</Navbar.Brand>
            <Navbar.Toggle aria-controls='basic-navbar-nav' />
            <Navbar.Collapse id='basic-navbar-nav'>
              <Nav className='mr-auto'>
                <Nav.Link as={Link} to="/">Home</Nav.Link>
                <NavDropdown title='Cadastros' id='basic-nav-dropdown'>
                  <NavDropdown.Item as={Link} to='/alunos'>Alunos</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to='/gerentes'>Gerentes</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to='/livros'>Livros</NavDropdown.Item>
                  <NavDropdown.Item as={Link} to='/revistas'>Revistas</NavDropdown.Item>
                </NavDropdown>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          <Switch>
            <Route path="/" exact={true} component={Home} />
            <Route path="/alunos" component={Alunos} />
            <Route path="/gerentes" component={Gerentes} />
            <Route path="/livros" component={Livros} />
            <Route path="/revistas" component={Revistas} />
          </Switch>
        </BrowserRouter>

      </Container>
    );
  }
}

export default App;